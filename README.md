# High Fidelity Localhost Connecter (hellcat) #

The High Fidelity localhost connecter (also know as hellcat) is a connector that allows for additional capability and features to be available in High Fidelity's Javascript and locally hosted content.

## Features ##

* Local WebServer to be able to host / test content
* Profiles to allow for multiple settings (for plugins) to be saved.
* Able to be accessed within High Fidelity (VR) and also the desktop with the same experience.

## Plugins ##

More information can be found at the [wiki](https://bitbucket.org/thecollectivesl/hellcat/wiki/)

### Who do I talk to? ###

* [Kurtis Anatine](https://keybase.io/theguywho)