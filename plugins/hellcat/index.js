/*
    hellcat base module

    If you have any questions get in contact with me at http://keybase.io/theguywho 
    If they can't back with a hash you better dash!
*/
const storage = require('electron-json-storage');

global.hellcat = {
    alert: []
}

exports.getAlert = function (args, callback) {
    callback({ alert: global.hellcat.alert});
    global.hellcat.alert = [];
};

exports.getProfile = function (args, callback) {
    storage.get('hellcat-settings', function(error, data){
        if (error) {
            callback({err: 'could not access settings.'});
        }
        try {
            callback({profileID: global.settings.profileID, settings: data[global.settings.profileID]});
        } catch (e) {
            callback({err: 'Error getting profile settings.'});
        }
        
    });
};

exports.setProfile = function (args, callback) {
    storage.get('hellcat-settings', function(error, data){
        if (error) {
            callback({err: 'could not access settings.'});
        }
        if (typeof(data[args.profileID]) !== 'undefined') {
            global.settings.profileID = args.profileID;
            global.settings.profileName = data[args.profileID].profileName;
            callback({profileID: global.settings.profileID, profileName: global.settings.profileName});
        } else {
            callback({err: 'profile does not exist.'});
        }
    });
};

exports.listProfile = function (args, callback) {
    storage.get('hellcat-settings', function(error, data){
        if (error) {
            callback({err: 'could not access settings.'});
        }
        try {
            if (args.index * args.amount > +Object.keys(data).length) {
                args.index = 0;
            }
            if (args.index === -1) {
                var trueIndex = Object.keys(data).length / args.amount;
                args.index = Math.floor(trueIndex);
                if (args.index === trueIndex) {
                    args.index -= 1;
                }
            }
            var returnValue = {
                index: args.index,
                amount: args.amount,
                data: {}
            };
            for(var i = args.index * args.amount; i < (args.index * args.amount)+args.amount && i < Object.keys(data).length; i++) {
                returnValue.data[Object.keys(data)[i]] = data[Object.keys(data)[i]];
            }
            callback(returnValue);
        } catch (e) {
            callback({err: 'could not access settings.'});
        }
    });
};

exports.newProfile = function (args, callback) {
    storage.get('hellcat-settings', function(error, data){
        if (error) {
            callback({err: 'could not access settings.'});
        }
        try {
            var newIndex = parseInt(Object.keys(data)[Object.keys(data).length - 1]) + 1;
            data[newIndex] = {
                profileName: args.profileName
            }
            storage.set('hellcat-settings', data, function(error) {
                if (error) {
                    callback({err: 'Could Not save profile'});
                } else {
                    callback({saved: true});
                }
            });
        } catch (e) {
            callback({err: 'Error saving profile.'});
        }
    });
};

exports.editProfile = function (args, callback) {
    storage.get('hellcat-settings', function(error, data){
        if (error) {
            callback({err: 'could not access settings.'});
        }
        try {
            if (typeof(data[args.profileID]) !== 'undefined') {
                for (var i in args.settings) {
                    data[args.profileID][i] = args.settings[i];
                }
                storage.set('hellcat-settings', data, function(error) {
                    if (error) {
                        callback({err: 'Could Not save profile.'});
                    } else {
                        callback({saved: true});
                    }
                });
            } else {
                callback({err: 'profile does not exist.'});
            }
        } catch (e) {
            callback({err: 'Error editing profile.'});
        }
    });
};