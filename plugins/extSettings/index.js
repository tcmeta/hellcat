/*
    hellcatSave - Settings Save module

    const extSettings = require('/extSettings/index.js');

    extSettings.save('setting','value', callback)
    extSettings.load('setting', callback)

    If you have any questions get in contact with me at http://keybase.io/theguywho 
    If they can't back with a hash you better dash!
*/
const storage = require('electron-json-storage');

exports.save = function (args, callback) {
    storage.set(global.settings.profileID+'-extSettings-'+args.name, {value: args.json}, function(error){
        if (error) {
            callback({err: error.message});
        } else {
            callback({set: true});
        }
    });
};

exports.load = function (args, callback) {
    storage.get(global.settings.profileID+'-extSettings-'+args.name, function(error, data){
        if (error) {
            callback({err: error.message});
        } else {
            if (typeof(data.value) === 'undefined') {
                data.value = '';
            }
            callback(data);
        }
    });
};
