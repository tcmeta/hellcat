/* 
    High Fidelity localhost (hellcat)
    Made by Kurtis Anatine (Vargink)

    This is a tool that runs locally that allows for things to be done in High Fidelity
*/
const {app, Tray, Menu, shell, clipboard, BrowserWindow} = require('electron');
const fs = require('fs');
const path = require('path');
const url = require('url');
const http = require('http');
const storage = require('electron-json-storage');
global.myDebug = true;
// Debug
function debug(message) {
    if (global.myDebug) {
        console.log(message);
    }
}
// Variables
var iconPath = path.join(__dirname, '/app/icons/hellcat.png');
var htmlDir = app.getPath('userData')+'/html';
let appIcon = null;
var plugins = {};
let win = null;
var isQuit = false;
// Save Settings
function save() {
    storage.set('settings', global.settings, function(error) {
        if (error) throw error;
    });
}
// Toggle Window
function windowToggle() {
    if (win.isVisible()) {
        win.hide();
    } else {
        win.loadURL('http://'+global.settings.hostName+':'+global.settings.port+'/app/hellcat/v1/html/hellcat.html');
        win.show();
    }
}
// Make the HTTP DIR
fs.exists(htmlDir, function (exist) {
    if(!exist) {
        fs.mkdirSync(htmlDir);
    }    
});
// HTTP Server
var server = http.createServer( function(req, res) {
    console.dir(req.param);
    if (req.method == 'POST') {
        debug("POST");
        var body = '';
        req.on('data', function (data) {
            body += data;
            debug("Partial body: " + body);
        });
        req.on('end', function () {
            debug("Body: " + body);
            res.writeHead(200, {'Content-Type': 'text/html'});
            try {
                body = JSON.parse(body);
                if (typeof(plugins[body['plugin']]) !== 'undefined') {
                    if (body['cmd']) {
                        plugins[body['plugin']][body['cmd']](body['args'], function(result) {
                            debug('result: '+JSON.stringify(result));
                            res.write(JSON.stringify(result));
                            res.end();
                        });
                    } else if (body['value']) {
                        
                    }   
                } else {
                    res.write(JSON.stringify({err: body['plugin']+' is not found'}));
                    res.end();
                }
            } catch (e) {
                res.write(JSON.stringify({err: 'try error: '+e.message}));
                res.end();
            }
        });
    }
    else
    {
        debug("GET");
        const parsedUrl = url.parse(req.url);
        let pathname = `${parsedUrl.pathname}`;
        // Check directory
        if (pathname.indexOf('/app/') !== -1) {
            pathname = '.'+pathname;
        } else {
            pathname = htmlDir+pathname;
        }
        const ext = path.parse(pathname).ext;
        const map = {
            '.ico': 'image/x-icon',
            '.html': 'text/html',
            '.js': 'text/javascript',
            '.json': 'application/json',
            '.css': 'text/css',
            '.png': 'image/png',
            '.jpg': 'image/jpeg',
            '.wav': 'audio/wav',
            '.mp3': 'audio/mpeg',
            '.svg': 'image/svg+xml',
            '.pdf': 'application/pdf',
            '.doc': 'application/msword'
        };
        fs.exists(pathname, function (exist) {
            if(!exist) {
              // if the file is not found, return 404
              res.statusCode = 404;
              res.end(`File ${pathname} not found!`);
              return;
            }
            // if is a directory search for index file matching the extention
            if (fs.statSync(pathname).isDirectory()) pathname += '/index' + ext;
            // read file from file system
            fs.readFile(pathname, function(err, data){
              if(err){
                res.statusCode = 500;
                res.end(`Error getting the file: ${err}.`);
              } else {
                // if the file is found, set Content-type and send data
                res.setHeader('Content-type', map[ext] || 'text/plain' );
                res.end(data);
              }
            });
        });
    }
});
// load the plugins!
fs.readdirSync('./plugins/').forEach(function(file) {
    try {
        debug('loading '+file);
        plugins[file] = require("./plugins/"+file);
    } catch (e) {
        debug('error loading '+file+': '+e.message);
    }
});

app.on('ready',function() {
    debug('Starting Electron Side');
    // Plugins and webserver should be loaded
    // load the user settings. Bail if we cant even get that
    storage.get('settings', function(error, data) {
        if (JSON.stringify(data) === JSON.stringify({}) || error) {
            global.settings = {
                version: 1,
                profileName: 'default',
                profileID: 1000,
                hostName: '127.0.0.1',
                port: 3030
            };
            save();
        } else {
            global.settings = data;
        }
        // Now we need to make sure the profile index exists
        storage.get('hellcat-settings', function(error, data){
            if (JSON.stringify(data) === JSON.stringify({}) || error) {
                var settings = {};
                settings[global.settings.profileID] = {
                    profileName: global.settings.profileName
                };
                storage.set('hellcat-settings', settings, function(error) {
                    if (error) throw error;
                });
            }
        });
        // Start the HTTP server
        server.listen(global.settings.port, global.settings.hostName);
        debug('Listening at http://'+global.settings.hostName+':'+global.settings.port);
        // Browser window
        win = new BrowserWindow({
            width: 800, 
            height: 600,
            show: false,
            icon: iconPath,
            frame: false
        });
        win.on('close', function(e) {
            if (isQuit) {
                win = null;
            } else {
                e.preventDefault();
                win.hide();
            }
        });
        // app icon
        appIcon = new Tray(iconPath);
        var contextMenu = Menu.buildFromTemplate([
            {
                label: 'Open',
                click: function() {
                    win.show();
                }
            },
            {
                label: 'Copy Hellcat app to clipboard',
                click: function() {
                    clipboard.writeText('http://'+global.settings.hostName+':'+global.settings.port+'/app/hellcat/v1/app-hellcat.js');
                }
            },
            {
                label: 'Copy Hellcat URL to clipboard',
                click: function() {
                    clipboard.writeText('http://'+global.settings.hostName+':'+global.settings.port);
                }
            },
            {
                label: 'Open Web Directory',
                click: function() {
                    shell.openItem(htmlDir);
                }
            },
            {
                label: 'Exit',
                click: function() {
                    isQuit = true;
                    app.quit();
                }
            }
        ]);
        appIcon.on('click', function(e) {
            windowToggle();
        });
        appIcon.setToolTip("Hellcat");
        appIcon.setContextMenu(contextMenu);
    });
});