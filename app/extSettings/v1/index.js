/*
    External Settings Module

*/
module.exports = {
    setValue: function(setting, option) {
        var http = new XMLHttpRequest();
        http.open("POST", "http://127.0.0.1:3030", false);
        http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        http.send(JSON.stringify({plugin: 'extSettings', cmd: 'save', args: {name: setting, json: option}}));
        if (http.status === 200) {
            var returnValue = JSON.parse(http.responseText);
            return returnValue.value;
        } 
    },
    getValue: function(setting) {
        var http = new XMLHttpRequest();
        http.open("POST", "http://127.0.0.1:3030", false);
        http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        http.send(JSON.stringify({plugin: 'extSettings', cmd: 'load', args: {name: setting}}));
        if (http.status === 200) {
            var returnValue = JSON.parse(http.responseText);
            return returnValue.value;
        } else {
            return '';
        }
    }
};