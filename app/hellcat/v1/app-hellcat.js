/*
    Kurtis's Hellcat Client

    If you have any questions get in contact with me at http://keybase.io/theguywho 
    If they can't back with a hash you better dash!

*/
(function() {
    // modules
    var extSettings = Script.require('http://127.0.0.1:3030/app/extSettings/v1/index.js'); 
    var hellcat = Script.require('http://127.0.0.1:3030/app/hellcat/v1/index.js');
    // Global Object
    var app = {
        name: "TC_Hellcat",
        version: 1,
        buttonName: 'Hellcat',
        iconNormal: Script.resolvePath("html/img/hellcat.svg"),
        iconEvent: Script.resolvePath("html/img/hellcat.svg"),
        iconActive: Script.resolvePath("html/img/hellcat.svg"),
        tabletURL: Script.resolvePath("html/hellcat.html"),
        open: false,
        alert: false,
        alertInterval: null
    };
    // Debug
    function debug(message) {
        if (Settings.getValue(app.name+'/debug')) {
            print(message);
        }
    }
    // Tablet
    var tablet = Tablet.getTablet("com.highfidelity.interface.tablet.system");
    var button = tablet.addButton({
        text: app.buttonName,
        icon: app.iconNormal,
        activeIcon: app.iconActive
    });
    // On Closing
    Script.scriptEnding.connect(function() {
        debug('Script Ending');
        tablet.removeButton(button);
        if (app.open) {
            tablet.webEventReceived.disconnect(webEvents);
        }
        Script.clearInterval(app.alertInterval);
    });
    // Load settings!
    var settings = extSettings.getValue('hellcat');
    if (settings === '') {
        settings = {
            version: 1,
            runScripts: true,
            scripts: []     
        };
        extSettings.setValue('hellcat', settings);
    }
    // Functions
    function webEvents(event) {
        
    }
    // Init stuff
    function start() {
        debug('Script Start');
        // when the url has changed
        tablet.screenChanged.connect(function(type, url) {
            debug('Tablet Screen Changed');
            if (type === 'Web' && url === app.tabletURL) {
                // the tablet is running your tabletURL
                if (!app.open) {
                    tablet.webEventReceived.connect(webEvents);
                }
                app.open = true;
            } else {
                // the url has changed
                if (app.open) {
                    tablet.webEventReceived.disconnect(webEvents);
                }
                app.open = false;
            }
        });
        button.clicked.connect(function() {
            debug('Tablet Button Clicked');
            if (!app.open) {
                tablet.gotoWebScreen(app.tabletURL);
            } else {
                tablet.gotoHomeScreen();
            }
        });
        app.alertInterval = Script.setInterval(function () {
            var newAlert = hellcat.getAlert();
            try {
                if (newAlert.alert.length) {
                    // Theres a new alert
                }
            } catch (e) {
                // problem
            }
        }, 5000);
    }
    // Starting Part
    start();
}());