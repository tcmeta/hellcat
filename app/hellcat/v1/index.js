/*
    Hellcat Module

*/
module.exports = {
    getAlert: function() {
        var http = new XMLHttpRequest();
        http.open("POST", "http://127.0.0.1:3030", false);
        http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        http.send(JSON.stringify({plugin: 'hellcat', cmd: 'getAlert', args: ''}));
        if (http.status === 200) {
            var returnValue = JSON.parse(http.responseText);
            return returnValue;
        } 
    },
    getProfile: function() {
        var http = new XMLHttpRequest();
        http.open("POST", "http://127.0.0.1:3030", false);
        http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        http.send(JSON.stringify({plugin: 'hellcat', cmd: 'getProfile', args: ''}));
        if (http.status === 200) {
            var returnValue = JSON.parse(http.responseText);
            return returnValue;
        } 
    },
    setProfile: function(profileID) {
        var http = new XMLHttpRequest();
        http.open("POST", "http://127.0.0.1:3030", false);
        http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        http.send(JSON.stringify({plugin: 'hellcat', cmd: 'setProfile', args: {profileID: profileID}}));
        if (http.status === 200) {
            var returnValue = JSON.parse(http.responseText);
            return returnValue;
        } 
    },
    newProfile: function(profileName) {
        var http = new XMLHttpRequest();
        http.open("POST", "http://127.0.0.1:3030", false);
        http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        http.send(JSON.stringify({plugin: 'hellcat', cmd: 'newProfile', args: {profileName: profileName}}));
        if (http.status === 200) {
            var returnValue = JSON.parse(http.responseText);
            return returnValue;
        } 
    },
    listProfile: function(amount, index) {
        var http = new XMLHttpRequest();
        http.open("POST", "http://127.0.0.1:3030", false);
        http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        http.send(JSON.stringify({plugin: 'hellcat', cmd: 'listProfile', args: {index: index, amount: amount}}));
        if (http.status === 200) {
            var returnValue = JSON.parse(http.responseText);
            return returnValue;
        }
    },
    editProfile: function(profileID, settings) {
        var http = new XMLHttpRequest();
        http.open("POST", "http://127.0.0.1:3030", false);
        http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        http.send(JSON.stringify({plugin: 'hellcat', cmd: 'editProfile', args: {profileID: profileID, settings: settings}}));
        if (http.status === 200) {
            var returnValue = JSON.parse(http.responseText);
            return returnValue;
        }
    }
};